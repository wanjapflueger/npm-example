/**
 * Example function
 * @param name Any name
 * @returns Example outpout
 * @example
 *   Example('Bob')
 */
export const Example = (name: string) => `Hello ${name}!`;
