# npm-example

Example files for npm publish on npmjs.com.

[![Pipeline status](https://gitlab.com/wanjapflueger/npm-example/badges/master/pipeline.svg)](https://gitlab.com/wanjapflueger/npm-example/-/pipelines)
![npm](https://img.shields.io/npm/v/@wanjapflueger/npm-example)
![npm bundle size](https://img.shields.io/bundlephobia/min/@wanjapflueger/npm-example)

---

## Table of contents

<!-- TOC -->

- [npm-example](#npm-example)
  - [Table of contents](#table-of-contents)
  - [Install](#install)
  - [Usage](#usage)
  - [Publish new version](#publish-new-version)
  - [Credits](#credits)

<!-- /TOC -->

---

## Install

```
npm i @wanjapflueger/npm-example
```

## Usage

```js
var Example = require('@wanjapflueger/npm-example');
Example.Example('Bob');

// or

import { Example } from '@wanjapflueger/npm-example';
Example('Bob');
```

## Publish new version

```bash
# 1.) Update changelog.md

# 2.) Update version

# 1.0.0 -> 1.0.1
npm version patch

# 1.0.0 -> 1.1.0
npm version minor

# 1.0.0 -> 2.0.0
npm version major

# 3.) Publish

npm publish
```

## Credits

- https://itnext.io/step-by-step-building-and-publishing-an-npm-typescript-package-44fe7164964c
- https://stackoverflow.com/questions/54775558/how-to-run-a-test-in-gitlab-for-nodejs-application/54775747#answer-54775747
